---
layout: post
title: "return and break"
description: ""
category: 
tags: []
---
{% include JB/setup %}


如果直接return，会跳出函数。
如果break，跳出这个函数的循环，并且循环后面的功能都不会执行
continue时，系统会跳过这一次的循环，进入下一次循环