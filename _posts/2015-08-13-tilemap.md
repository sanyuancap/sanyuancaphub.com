---
layout: post
title: "tilemap"
description: ""
category: 
tags: []
---
{% include JB/setup %}

##tilemap 瓦片大小
瓦片可以使任意大小，长宽可以不同，但是不同图块文件，需要放在不同的层，否则会出现图块错乱的情况
![参考图片](https://github.com/sanyuancap/myBlogPic/blob/master/tilemap/tilemap1.png)

![参考图片](https://github.com/sanyuancap/myBlogPic/blob/master/tilemap/tilemap2.png)

![参考图片](https://github.com/sanyuancap/myBlogPic/blob/master/tilemap/tilemap3.png)