---
layout: post
title: "chukongCocosCodeIDE"
description: ""
category: 
tags: []
---
{% include JB/setup %}

##cocos code IDE关于lua的代码补全
虽然说官方最新版本说明，intel版本的还没有lua代码补全，下个版本会发布，所以lua开发者只有用1.2版本。但是实践出不管是哪个版本，都根本没有补全一说，只有js补全。
网上有[添加用户自定义库的代码补全](http://blog.csdn.net/qqmcy/article/details/40149593)，但是IDE的版本之间相差太大，找不到添加入口，就算添加进去，也无法补全，只有不停的闪退和卡顿。

耽误时间确实让人恼火，国内的cocos2dx不成熟，background和backGround接口都无法统一，身为一个游戏引擎也是很少有的事。

只能希望国内的技术尽快强大起来，多铺点路，少挖点坑。