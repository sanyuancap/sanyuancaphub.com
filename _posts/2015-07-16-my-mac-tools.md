---
layout: post
title: "my mac tools"
description: ""
category: 
tags: []
---
{% include JB/setup %}
## tools lists 
 - [Sublime Text(markdown插件+theme)][ST]
 - Iterm(theme)
 - [git][git]
 - Cornerstone
 - sourcetree
 - [homebrew cask zsh wget][hcz] 
 - android file transfer
 - GlyphDesigner 
 - ParticleDesigner
 - ImageAlpha

## develop tools
 - [webstorm][ws]
 - texturepacker
 - Xcode
 - eclipse
 - [Cocos Studio][cs]
 - Cocos Code IDE

##efficient
 - magican
 - evernote
 - firefox
 - iToolsPro
 - baidupan
 - namechanger
 - filemerge（mac self）
 - [photoshop](http://www.nowmac.com/soft/design/graphic/Photoshop-CS6.html)
 - [officeForMac2016](http://www.chinamac.com/download/mac7059.html)
 - [tuxerantfs](http://bbs.feng.com/read-htm-tid-5172799.html)
 - [MxVPN](http://www.mxvpnjsq.info/home.php?mod=spacecp&ac=profile&op=password)

[ST]:http://www.cnblogs.com/IPrograming/p/Sublime-markdown-editor.html
[hcz]:http://my.oschina.net/evilgod528/blog/306548
[ws]:http://blog.csdn.net/qinning199/article/details/40395085#0-tsina-1-91169-397232819ff9a47a7b7e80a40613cfe1
[cs]:http://www.cocos.com/download/
[git]:http://www.cnblogs.com/ccdev/archive/2012/09/12/2682098.html